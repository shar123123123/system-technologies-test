﻿using DatabaseSQL.Services.EntiryFrameworkCore.Entities;
using DatabaseSQL.Services.EntiryFrameworkCore.InterfaceServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DepartmentController : ControllerBase
    {
        public readonly IDepartmentService departmentService;

        public DepartmentController(IDepartmentService departmentService)
        {
            this.departmentService = departmentService;
        }

        [HttpPost]
        public Task<int> CreateDepartment(Department department)
            => this.departmentService.CreateDepartmentAsync(department);

        [HttpPost("{departmentId}/")]
        public Task<int> AddDepartmentEmployee(int departmentId, int employeeId)
            => this.departmentService.AddDepartmentEmployee(departmentId, employeeId);

        [HttpGet]
        public Task<IList<Department>> ShowDepartments(int offset, int limit)
            => this.departmentService.ShowDepartmentsAsync(offset, limit);

        [HttpPut]
        public Task<bool> UpdateDepartment(int departmentId, Department department)
            => this.departmentService.UpdateDepartmentAsync(departmentId, department);

        [HttpDelete]
        public Task<bool> DeleteDepartment(int departmentId)
            => this.departmentService.DestroyDepartmentAsync(departmentId);
    }
}
