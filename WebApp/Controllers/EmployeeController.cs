﻿using DatabaseSQL.Services.EntiryFrameworkCore.InterfaceServices;
using DatabaseSQL.Services.EntiryFrameworkCore.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeeController : ControllerBase
    {
        public readonly IEmployeeService employeeService;
        public EmployeeController(IEmployeeService employeeService)
        {
            this.employeeService = employeeService;
        }

        [HttpGet]
        public Task<IList<Employee>> ShowEmployees(int offset, int limit)
            => this.employeeService.ShowEmployeesAsync(offset, limit);

        [HttpPost]
        public Task<int> CreateEmployee(Employee employee)
            => this.employeeService.CreateEmployeeAsync(employee);

        [HttpPut]
        public Task<bool> UpdateEmployee(int employeeId, Employee employee)
            => this.employeeService.UpdateEmployeeAsync(employeeId, employee);

        [HttpDelete]
        public Task<bool> DeleteEmployee(int employeeId)
            => this.employeeService.DestroyEmployeeAsync(employeeId);
    }
}
