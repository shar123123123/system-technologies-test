﻿    using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseSQL.Services.EntiryFrameworkCore.Entities
{
    public class Department
    {
        [Key]
        public int DepartmentId { get; set; }

        public string DepartmentName { get; set; }

        public DateTime DateOfAdd { get; set; }

        public DateTime DateOfChange { get; set; }

        public ICollection<Employee> Employees { get; set; }
    }
}
