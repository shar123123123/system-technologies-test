﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseSQL.Services.EntiryFrameworkCore.Entities
{
    public class Employee
    {
        [Key]
        public int EmployeeId { get; set; }

        public string FistName { get; set; }

        public string LastName { get; set; }

        public DateTime DateOfAdd { get; set; }

        public DateTime DateOfChange { get; set; }

        public DateTime StartToWork { get; set; }

        public string Position { get; set; }

        public int? DepartmentId { get; set; }

        public Department Department { get; set; }
    }
}
