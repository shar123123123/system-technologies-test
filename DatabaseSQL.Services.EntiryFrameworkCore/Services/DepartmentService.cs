﻿using DatabaseSQL.Services.EntiryFrameworkCore.Context;
using DatabaseSQL.Services.EntiryFrameworkCore.Entities;
using DatabaseSQL.Services.EntiryFrameworkCore.InterfaceServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseSQL.Services.EntiryFrameworkCore.Services
{
    public class DepartmentService : IDepartmentService
    {
        private readonly SqlContext context;

        public DepartmentService(SqlContext sqlContext)
        {
            this.context = sqlContext;
        }

        public async Task<int> AddDepartmentEmployee(int departmentId, int employeeId)
        {
            var department = await this.context.Departments.FindAsync(departmentId);
            var employee = await this.context.Employees.FindAsync(employeeId);

            if (department is null)
            {
                throw new ArgumentNullException($"{department} is not exists.");
            }

            if (employee is null)
            {
                throw new ArgumentNullException($"{employee} is not exists.");
            }

            this.context.Entry(department).Collection(d => d.Employees).Load();

            department.DateOfChange = DateTime.Now;
            employee.DateOfChange = DateTime.Now;
            employee.StartToWork = DateTime.Now;
            employee.DateOfAdd = DateTime.Now;
            department.Employees.Add(employee);
            await this.context.SaveChangesAsync();
            return employeeId;
        }

        public async Task<int> CreateDepartmentAsync(Department department)
        {
            department.DateOfAdd = DateTime.Now;
            department.DateOfChange = DateTime.Now;
            this.context.Departments.Add(department);
            await this.context.SaveChangesAsync();
            return department.DepartmentId;
        }

        public async Task<bool> DestroyDepartmentAsync(int departmentId)
        {
            var department = await this.context.Departments.FindAsync(departmentId);

            if (department is null)
            {
                throw new ArgumentNullException($"{department} is not exists.");
            }

            this.context.Entry(department).Collection(d => d.Employees).Load();
            this.context.Departments.Remove(department);
            await this.context.SaveChangesAsync();
            return !(department is null);
        }

        public async Task<IList<Department>> ShowDepartmentsAsync(int offset, int limit)
            => await this.context.Departments.Skip(offset).Take(limit).ToListAsync();

        public async Task<(bool, Department)> TryShowDepartmentAsync(int departmentId)
        {
            var department = await this.context.Departments.FindAsync(departmentId);

            if (department is null)
            {
                throw new ArgumentNullException($"{department} is not exists.");
            }

            return (!(department is null), department);
        }

        public async Task<bool> UpdateDepartmentAsync(int departmentId, Department department)
        {
            var depart = await this.context.Departments.FindAsync(departmentId);

            if (depart is null)
            {
                throw new ArgumentNullException($"{depart} is not exists.");
            }

            depart.DateOfChange = DateTime.Now;
            depart.DepartmentName = department.DepartmentName;

            await this.context.SaveChangesAsync();
            return true;
        }
    }
}
