﻿using DatabaseSQL.Services.EntiryFrameworkCore.Entities;
using DatabaseSQL.Services.EntiryFrameworkCore.InterfaceServices;
using DatabaseSQL.Services.EntiryFrameworkCore.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DatabaseSQL.Services.EntiryFrameworkCore.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly SqlContext context;

        public EmployeeService(SqlContext sqlContext)
        {
            this.context = sqlContext;
        }

        public async Task<int> CreateEmployeeAsync(Employee employee)
        {
            employee.DateOfChange = DateTime.Now;
            employee.DateOfAdd = DateTime.Now;
            this.context.Employees.Add(employee);
            await this.context.SaveChangesAsync();
            return employee.EmployeeId;
        }

        public async Task<bool> DestroyEmployeeAsync(int employeeId)
        {
            var employee = await this.context.Employees.FindAsync(employeeId);

            if (employee is null)
            {
                throw new ArgumentNullException($"{employee} is not exists.");
            }

            this.context.Entry(employee).Reference(e => e.Department).Load();
            this.context.Employees.Remove(employee);
            await this.context.SaveChangesAsync();
            return !(employee is null);
        }

        public async Task<IList<Employee>> LookupEmployeesByNameAsync(IList<string> names)
            => await this.context.Employees.Where(emp => names.Contains($"{emp.FistName} {emp.LastName}")).ToListAsync();

        public async Task<IList<Employee>> ShowEmployeesAsync(int offset, int limit)
            => await this.context.Employees.Skip(offset).Take(limit).ToListAsync();

        public async Task<(bool, Employee)> TryShowEmployeeAsync(int employeeId)
        {
            var employee = await this.context.Employees.FindAsync(employeeId);

            if (employee is null)
            {
                throw new ArgumentNullException($"{employee} is not exists.");
            }

            return (!(employee is null), employee);
        }

        public async Task<bool> UpdateEmployeeAsync(int employeeId, Employee employee)
        {
            var empFind = await this.context.Employees.FindAsync(employeeId);

            if (employee is null)
            {
                throw new ArgumentNullException($"{employee} is not exists.");
            }

            empFind.FistName = employee.FistName;
            empFind.LastName = employee.LastName;
            empFind.Position = employee.Position;
            empFind.Department = employee.Department;
            empFind.DepartmentId = employee.DepartmentId;
            empFind.DateOfChange = DateTime.Now;
            await this.context.SaveChangesAsync();
            return true;
        }
    }
}
