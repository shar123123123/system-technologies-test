﻿using DatabaseSQL.Services.EntiryFrameworkCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseSQL.Services.EntiryFrameworkCore.InterfaceServices
{
    public interface IDepartmentService
    {
        /// <summary>
        /// Shows a list of departments using specified offset and limit for pagination.
        /// </summary>
        /// <param name="offset">An offset of the first element to return.</param>
        /// <param name="limit">A limit of elements to return.</param>
        /// <returns>A <see cref="IList{T}"/> of <see cref="Department"/>.</returns>
        Task<IList<Department>> ShowDepartmentsAsync(int offset, int limit);

        /// <summary>
        /// Try to show a department with specified identifier.
        /// </summary>
        /// <param name="employeeId">A department identifier.</param>
        /// <returns>Returns true if a department is returned; otherwise false.</returns>
        Task<(bool, Department)> TryShowDepartmentAsync(int departmentId);

        /// <summary>
        /// Creates a new department.
        /// </summary>
        /// <param name="employee">A <see cref="Department"/> to create.</param>
        /// <returns>An identifier of a created department.</returns>
        Task<int> CreateDepartmentAsync(Department department);

        /// <summary>
        /// Destroys an existed department.
        /// </summary>
        /// <param name="categoryId">A department identifier.</param>
        /// <returns>True if a department is destroyed; otherwise false.</returns>
        Task<bool> DestroyDepartmentAsync(int departmentId);

        /// <summary>
        /// Updates a Department.
        /// </summary>
        /// <param name="departmentId">A department identifier.</param>
        /// <returns>True if a department is updated; otherwise false.</returns>
        Task<bool> UpdateDepartmentAsync(int departmentId, Department department);

        /// <summary>
        /// Add employee to department.
        /// </summary>
        /// <param name="departmentId">A department identifier.</param>
        /// <returns>id employee.</returns>
        Task<int> AddDepartmentEmployee(int departmentId, int employeeId);
    }
}
