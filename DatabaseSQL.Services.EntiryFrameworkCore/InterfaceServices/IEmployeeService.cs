﻿using DatabaseSQL.Services.EntiryFrameworkCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseSQL.Services.EntiryFrameworkCore.InterfaceServices
{
    public interface IEmployeeService
    {
        /// <summary>
        /// Shows a list of employee using specified offset and limit for pagination.
        /// </summary>
        /// <param name="offset">An offset of the first element to return.</param>
        /// <param name="limit">A limit of elements to return.</param>
        /// <returns>A <see cref="IList{T}"/> of <see cref="Employee"/>.</returns>
        Task<IList<Employee>> ShowEmployeesAsync(int offset, int limit);

        /// <summary>
        /// Try to show a employee with specified identifier.
        /// </summary>
        /// <param name="employeeId">A employee category identifier.</param>
        /// <returns>Returns true if a employee is returned; otherwise false.</returns>
        Task<(bool, Employee)> TryShowEmployeeAsync(int employeeId);

        /// <summary>
        /// Creates a new employee.
        /// </summary>
        /// <param name="employee">A <see cref="Employee"/> to create.</param>
        /// <returns>An identifier of a created employee.</returns>
        Task<int> CreateEmployeeAsync(Employee employee);

        /// <summary>
        /// Destroys an existed employee.
        /// </summary>
        /// <param name="categoryId">A employee identifier.</param>
        /// <returns>True if a employee is destroyed; otherwise false.</returns>
        Task<bool> DestroyEmployeeAsync(int employeeId);

        /// <summary>
        /// Looks up for Employee with specified names.
        /// </summary>
        /// <param name="names">A list of employee names.</param>
        /// <returns>A list of employee with specified names.</returns>
        Task<IList<Employee>> LookupEmployeesByNameAsync(IList<string> names);

        /// <summary>
        /// Updates a Employee.
        /// </summary>
        /// <param name="employeeId">A employee identifier.</param>
        /// <returns>True if a employee is updated; otherwise false.</returns>
        Task<bool> UpdateEmployeeAsync(int employeeId, Employee employee);
    }
}
